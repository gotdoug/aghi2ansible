# A Gentle Hands-on Intro 2 Ansible
[//]: #  "updated to prevent deletion"
## 2020-04-04

## The SFS Rap

[wrapper slides](wrapper-slides.fodp)

---

## Discussion 0: What is Ansible?

Compare shell, puppet, ansible

cf: https://jjasghar.github.io/blog/2015/12/20/chef-puppet-ansible-salt-rosetta-stone/

## [Check Requirements](labs/00-check-reqs.md)

- ssh
- web browser
- gitlab account and 1+ authorized keys

## [Infrastructure, Inventory, Pre-Ansible](labs/01-infrastructure/)

- create machines in AWS
- add myself as an admin, using the build key

## [Static Inventory](labs/02-inventory-static)

- create static inventory

## [Dynamic Inventory](labs/03-inventory-dynamic)

- create dynamic inventory
- add myself and my partner/s to my machines, using my own account
- and drop the build admin, to avoid abuse

## Discussion 1: What does "idempotent" mean?

What does it mean to say, "Good playbooks and modules are 'idempotent'?"

Note that the `aws-create-my-machines.yml` playbook is idempotent.
Try re-running it!

## [Ad-hoc Operations](labs/04-ad-hoc-ansible.md)

## Discussion 2: What is a playbook?

## [A Kinda Trifecta](labs/05-trifectish/)

- Use a playbook to install, configure, and start and enable a service

## XX - Wordpress or Ghost Playbook

## XX - Prometheus, Grafana, and Friends on Docker Compose

## Previous Exercise Plans

* [2017.05](ansible_aws/ansible.md)
* [2017.06](ansible_aws/Ansible_Class_Notes2.md)
* [The Old Slides](WomenWhoCodeANSIBLEv1.pptx)

## [Teacher Tools](teacher-tools/)
