## create a dynamic inventory

```bash
cd ~/sfs/aghi2ansible/labs/03-inventory-dynamic
cat ansible.cfg
cat aws_ec2.yml
ansible all --list
ansible-inventory --graph
# Remove 'Instructor' and add your usernames in a list to the tag:Owner filter
vim aws_ec2.yml
# Delete the inventory cache
rm aws_ec2_...  # hit tab

ansible all --list
ansible-inventory --graph
ansible tag_Owner_$USER -m ping
ansible tag_Owner_$USER -a uptime
```

## update admins and remove the build admin

Copy the dynamic inventory configs to the lab 01 directory and update admins.
But this time, run the playbook as yourself, and get rid of the build user to
prevent abuse of that account.

```bash
cp ansible.cfg ../01-infrastructure/
cp aws_ec2.yml ../01-infrastructure/
cd ../01-infrastructure/
ansible-playbook update-linux-admins.yml --extra-vars="target=tag_Owner_$USER"
```

Notice: This time, because you're running the playbook as your fully
integrated self, it gets rid of that pesky build user!
