# Installing Ansible

## Checking the Ansible version available

fedora:

```bash
dnf provides "*bin/ansible"
```

ubuntu:

```bash
apt update && apt -y install apt-file && apt-file update && apt-file search bin/ansible
apt search ansible
```

centos:

```bash
dnf -y install epel-release && dnf provides "*bin/ansible"
```

## Installing Ansible

Debian - Ubuntu - Mint - Etc...

```bash
sudo apt-get update && sudo apt-get install ansible
```

Fedora

```bash
sudo dnf install ansible
```

CentOS

```bash
sudo dnf install epel-release
sudo dnf install ansible
```

OpenSUSE

```bash
sudo zypper install ansible
```
