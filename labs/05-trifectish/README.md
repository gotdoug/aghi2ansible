# Trifecta!

Install a service, configure it, and start and enable it.

In this unit, we'll create and update a webserver.

```bash
cd ~/sfs/aghi2ansible/labs/05-trifectish/
cat ansible.cfg                 # note that we're using a static inventory
cat hostlist                    # note that we're defining a group named 'web'
echo "$PRIVATE_IP" >> hostlist  # add my machine
sed -e '/1.2.3.4/d' -i hostlist # remove the example machine
cat hostlist                    # check my work
```

Now that my inventory is right, Imma make a *webserver*!

```bash
ansible-playbook web.yml
```

Now, manually point your web-browser at $PUBLIC_IP and/or run one of the
following.

```bash
curl $PUBLIC_IP
xdg-open $PUBLIC_IP
```

Server's up, yo. But, the content sucks. Let's move in the template.

```bash
mv index.html.j2.findme index.html.j2
ansible-playbook web.yml
```

Aaaand, refresh it, or re-curl it, or whatever...

Now, update the template, and run the playbook again.

```bash
vim index.html.j2
ansible-playbook web.yml
```

Well, that's it. We'll run this class again in six months, with an even better capstone exercise, at quadruple the price, but *you*, you beautiful human, you get to come for free. Please bring a friend. And, please give feedback: www.sofree.us/links
