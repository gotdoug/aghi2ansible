#!/bin/bash -eu

# This script uses the first user account to add the admins

if [[ $# -ne 2 ]] && [[ $# -ne 3 ]]
then
    echo "Usage: $0 USER TARGET [KEYFILE]"
    echo "USER: the username of the admin user account already present."
    echo "TARGET: an actual, reachable name or address for the machine."
    echo "KEYFILE: (optional) the path to the admin's private key file."
    echo "If you don't specify a KEYFILE, you will be asked for a password."
    exit 1
fi

cd "$( dirname "$0" )"
user=$1
target=$2

if [[ $# -eq 3 ]]
then
    keyfile=$3
    # add admins to target using a keyfile
    ansible-playbook update-linux-admins.yml -i "$target, " --extra-vars="target='$target'" -u "$user" --key-file "$keyfile"
else
    # add admins to target using a password
    ansible-playbook update-linux-admins.yml -i "$target, " --extra-vars="target='$target'" -u "$user" -k -K
fi
