# Infrastructure

Get ready.

```bash
ssh -A ansible-controller.sofree.us
# or `ssh ansible-controller.sofree.us` if not forwarding an agent
# but make sure to start one on Ansible Controller, in that case
cd sfs... # tab twice
git pull
cd labs/01... # tab
```

## Create machines in AWS

```bash
# create my machines
ansible-playbook aws-create-my-machines.yml
# record the public and private ip addresses of your machines from the output
```

Add yourself and your partner/s as admins using the build account and key

```bash
# update the global var 'admins' with the usernames of yourself
# and your partner/s
vim group_vars/all.yml

# as 'centos', the build admin, using the build key,
# add yourself and your partner/s as admins
./add-linux-admins.sh centos $IP $USER-build.key
# examples:
# ./add-linux-admins.sh centos 172.31.55.78 dlwillson-build.key
# ./add-linux-admins.sh centos 44.226.204.32 dlwillson-build.key
```

From here on, you need an ssh key unlocked and loaded. Confirm.

```bash
# Confirm that you have at least one key on the ring.
ssh-add -L
```
